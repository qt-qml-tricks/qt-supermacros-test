#include "ClassWithProperties.hpp"

using namespace std;

ClassWithProperties::ClassWithProperties(QObject *parent) : QObject(parent)
{
    m_ptrProp = new QObject();
    m_sharedPtrProp = make_shared<QObject>();
    m_sharedPtrProp->setObjectName("name");
}
