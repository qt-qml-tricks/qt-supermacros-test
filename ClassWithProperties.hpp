#ifndef CLASSWITHPROPERTIES_HPP
#define CLASSWITHPROPERTIES_HPP

#include <QObject>
#include <QString>

#include "QQmlPtrPropertyHelpers.h"

class ClassWithProperties : public QObject
{
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY(QObject, ptrProp)
    QML_READONLY_SHARED_PTR_PROPERTY(QObject, sharedPtrProp)
public:
    explicit ClassWithProperties(QObject *parent = 0);

signals:

public slots:
};

#endif // CLASSWITHPROPERTIES_HPP
