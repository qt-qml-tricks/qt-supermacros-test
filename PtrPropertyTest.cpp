#include "gtest/gtest.h"

#include <QObject>
#include <QVariant>

#include "QQmlPtrPropertyHelpers.h"

#include "ClassWithProperties.hpp"

TEST(PtrProperty, notNull) {
    ClassWithProperties o;
    EXPECT_FALSE(o.property("ptrProp").isNull());
}

TEST(SharedPtrProperty, notNull) {
    ClassWithProperties o;
    EXPECT_FALSE(o.property("sharedPtrProp").isNull());
}

TEST(SharedPtrProperty, objectName) {
    ClassWithProperties o;
    EXPECT_EQ(o.property("sharedPtrProp").value<QObject*>()->objectName(), QString("name"));
}
