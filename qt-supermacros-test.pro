TEMPLATE = app

CONFIG += console c++11 thread
CONFIG -= app_bundle

QT += core

GOOGLE_TEST_SOURCE = $$(GOOGLE_TEST_ROOT)/source/googletest
GOOGLE_TEST_BUILD = $$(GOOGLE_TEST_ROOT)/build/googlemock/gtest

INCLUDEPATH += $${GOOGLE_TEST_SOURCE}
INCLUDEPATH += $${GOOGLE_TEST_SOURCE}/include

LIBS += -lgtest -L$${GOOGLE_TEST_BUILD}
LIBS += -lgtest_main -L$${GOOGLE_TEST_BUILD}

SOURCES += $${GOOGLE_TEST_SOURCE}/src/gtest-all.cc \
    PtrPropertyTest.cpp \
    ClassWithProperties.cpp

!include (../qt-supermacros/QtSuperMacros.pri){
    message("QtSuperMacros.pri is not included")
}

HEADERS += \
    ClassWithProperties.hpp
